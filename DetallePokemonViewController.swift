//
//  DetallePokemonViewController.swift
//  pokedex
//
//  Created by Christian Colman on 2022-11-02.
//

import UIKit

class DetallePokemonViewController: UIViewController {
    // variables
    var pokemonMostrar : Pokemon?
    
    
    // MARK: Outltes
    
    @IBOutlet weak var imagenPokemon: UIImageView!
    @IBOutlet weak var descripcionPokemon: UITextView!
    @IBOutlet weak var tipoPokemon: UILabel!
    @IBOutlet weak var ataquePokemon: UILabel!
    
    @IBOutlet weak var defensaPokemon: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Imagen a mostrar
        imagenPokemon.loadFrom(URLAddress: pokemonMostrar?.imageUrl ?? "")
        
        tipoPokemon.text = "Tipo : \(pokemonMostrar?.type ?? "")"
        ataquePokemon.text = "Ataque : \(pokemonMostrar!.attack )"
        defensaPokemon.text = "Defensa: \(pokemonMostrar!.defense )"
        descripcionPokemon.text = pokemonMostrar?.description ?? ""
        // Do any additional setup after loading the view.
    }
    

 

}


extension UIImageView {
    func loadFrom (URLAddress: String){
        guard let url = URL(string: URLAddress) else {return}
        DispatchQueue.main.async { [weak self] in
            if let imagenData = try? Data(contentsOf: url){
                if let loadedImage = UIImage (data: imagenData){
                    self?.image = loadedImage
                }
            }
        }
    }
}
