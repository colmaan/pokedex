//
//  DatosPokemon.swift
//  pokedex
//
//  Created by Christian Colman on 2022-11-02.
//

import Foundation
struct Pokemon : Decodable, Identifiable { // Identifiable que es?
    let id : Int
    let attack : Int
    let defense : Int
    let description : String
    let name : String
    let imageUrl : String // Los datos deben llamarse exactamente igual que al del JSon
    let type : String
}
