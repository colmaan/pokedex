//
//  ViewController.swift
//  pokedex
//
//  Created by Bootcamp 3 on 2022-11-02.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var tablapokemon: UITableView!
    
    @IBOutlet weak var searchbarpokemon: UISearchBar!
    
    
    
  // MARK: Vriables
    var pokemonManager = PokemonManager()
    var pokemons : [Pokemon] = []
    var pokemonSeleccionado : Pokemon?
    var pokemonFiltrados: [Pokemon] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // registrar la nueva celda
        tablapokemon.register(UINib (nibName: "CeldaPokemonTableViewCell", bundle: nil), forCellReuseIdentifier: "celda")
        
        
        pokemonManager.delegado = self
        searchbarpokemon.delegate = self
        searchbarpokemon.searchTextField.textColor = UIColor.green
        
        tablapokemon.delegate = self
        tablapokemon.dataSource = self
        
        // Ejecutar el metodo para buscar la lista de pokmeon en la api
        
        pokemonManager.verPokemon()
        pokemonFiltrados = pokemons
    }


}








// MARK: SearchBar
extension ViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        pokemonFiltrados = []
        if searchText == "" {
            pokemonFiltrados = pokemons
        }else {
            for poke in pokemons {
                if poke.name.lowercased().contains(searchText.lowercased()){
                    pokemonFiltrados.append(poke)
                }
            }
        }
        self.tablapokemon.reloadData()
    }
}

// MARK: Delegado Pokemon
extension ViewController : pokemonManagerDelegado {
    func mostrarListaPokemon(lista: [Pokemon]) {
        pokemons = lista
        DispatchQueue.main.async {
            self.pokemonFiltrados = self.pokemons
            self.tablapokemon.reloadData()
        }
    }
    
    
}

extension ViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonFiltrados.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tablapokemon.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! CeldaPokemonTableViewCell
        celda.nombrePokemon.text = pokemonFiltrados[indexPath.row].name
        celda.ataquePokemon.text = "Ataque: \(pokemonFiltrados[indexPath.row].attack)"
        celda.defensaPokemon.text = "Defensa: \(pokemonFiltrados[indexPath.row].defense)"
        

        if let urlString = pokemonFiltrados[indexPath.row].imageUrl as? String {
            if let imagenURL = URL(string: urlString) {
                DispatchQueue.global().async {
                    guard let imagenData = try? Data (contentsOf: imagenURL) else
                    { return }
                    let image = UIImage(data: imagenData)
                    DispatchQueue.main.async {
                        celda.imagenPokemon.image = image
                    }
                }
            }
        }
        
        return celda
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pokemonSeleccionado = pokemonFiltrados[indexPath.row]
        
        performSegue(withIdentifier: "verPokemon", sender: self)
        
        tablapokemon.deselectRow(at: indexPath, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verPokemon" {
            let verPokemon = segue.destination as! DetallePokemonViewController
            verPokemon.pokemonMostrar = pokemonSeleccionado
        }
    }
}

